# 智慧小区管理系统

#### 介绍
使用SpringBoot+Mybatis+BootStrap+Layui+VUE制作的智慧小区物业管理系统。  
本系统采用了 B/S 架构，Java、Html、Css、Js 等技术，使用了主流的后台开发框架SpringBoot(SpringMVC+Spring+Mybatis)，前端开发框架使用了 LayUI、Vue、JQuery 以及 Vue
的前端组件库 Element-UI，采用了开源的轻量级数据库 Mysql 进行开发。实现了小区管理、房产管理、设备管理、业主管理、服务管理、车位管理等主要功能。提升物业管理水平，提高工作效率，降低成本，便于物业快速回收物业费；扩大服务范围和能力，方便住户，提升用户满意度；为物业公司扩展了新的收入渠道：租赁中介、社区团购、物业服务；为住户缴纳物业费、水电、停车费等生活费用，提供在线支付，方便快捷。


#### 安装教程

1.  导入项目到IDE工具（如Eclipse）
2.  导入数据库文件estatedb.sql到MySQL
3.  修改src/main/resources/application.yml文件中的数据库链接地址、账号密码
4.  运行启动类com.es.estatemanagement.EstateManagementApplication，启动项目，默认端口为8888
5.  浏览器访问http://localhost:8888/，进入首页



#### 使用说明

1. 系统上传的文件会存放在项目目录estate-management/src/main/resources/static/fileupload
2. 项目使用的MySQL驱动版本是5.1.30，如果MySQL版本是8.0以上，需要将pom.xml中的MySQL驱动版本改为8.0以上版本，或者直接将`<version>5.1.30</version>`删除即可。因为Springboot使用的默认MySQL驱动版本为8.0.20  
4. 小区管理模块实现了从前端到控制层、业务层、持久层的完整功能，可以参照该模块实现其他功能。
5. 本系统使用了Mybatis插件TkMapper，TkMapper又名通用 Mapper，封装了对单表的 CRUD 操作。通用 Mapper 可以极大的方便开发人员。可以随意的按照自己的需要选择通用方法，还可以很方便的开发自己的通用方法。

项目在eclipse中的截图：  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/101732_0b4b402d_5340558.png "屏幕截图.png")

项目部分页面截图：  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/103337_42b0422b_5340558.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/103426_e2ccd718_5340558.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/103820_1ead0b80_5340558.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/103936_41daeb24_5340558.png "屏幕截图.png")

关注下面的微信公众号，获取更多项目源码  
需要项目部署指导、毕业设计指导，可在下面微信公众号联系我：  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/102354_af621fea_5340558.png "屏幕截图.png")
