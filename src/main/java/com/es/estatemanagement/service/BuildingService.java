package com.es.estatemanagement.service;

import com.es.estatemanagement.domain.Building;
import com.es.estatemanagement.domain.Community;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

public interface BuildingService {

    public List<Building> findAll();

    public Page<Building> search(Map searchMap);

    public Boolean add(Building building);

    public Building findById(Integer id);

    public Boolean update(Building building);

    public Boolean del(List<Integer> ids);
}
