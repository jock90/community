package com.es.estatemanagement.dao;

import com.es.estatemanagement.domain.Building;
import com.es.estatemanagement.domain.Community;

import org.springframework.stereotype.Repository;

import tk.mybatis.mapper.common.Mapper;

@Repository
public interface BuildingMapper extends Mapper<Building> {
}
